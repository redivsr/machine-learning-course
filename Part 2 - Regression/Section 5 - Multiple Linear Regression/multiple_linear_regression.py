# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('50_Startups.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values

# preprocessing tools to encode state string to dummy variables
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
LabelEncoder_X = LabelEncoder()
X[:, 3] = LabelEncoder_X.fit_transform(X[:, 3])
onehotencoder = OneHotEncoder(categorical_features=[3])
X = onehotencoder.fit_transform(X).toarray()

# avoiding Dummy Variable Trap, remove first dummy variable
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# fit multiple linear regression to training set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

# predicting test data
y_pred = regressor.predict(X_test)
for i in range(len(y_pred)):
    print y_pred[i], y_test[i]

# Backward elimination
import statsmodels.formula.api as sm
X = np.append(arr = np.ones(shape = (50, 1), dtype=int), values = X, axis = 1)
X_opt = X[:, [0, 1, 2, 3, 4, 5]]

regressor_ols = sm.OLS(endog = y, exog = X_opt).fit()
regressor_ols.summary()

# remove column with P>significant level, refit and so on