# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Salary_Data.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 1].values

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.333, random_state = 0)

# Fitting Simple Linear Regression to the training set
# Linear regression calculates the min error sum from each training point to
# a linear function
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

# Predict test data
y_pred = regressor.predict(X_test)

# plot test data and prediction
plt.scatter(X_train, y_train, color='blue')
plt.plot(X_train, regressor.predict(X_train), color='red')
plt.legend(['Predicted', 'Real value'])
plt.xlabel('Experience (years)')
plt.ylabel('Salary')
plt.grid(which='minor')
plt.show()