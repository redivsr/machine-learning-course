# Apriori

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Data Preprocessing
dataset = pd.read_csv('/opt/machine-learning-course/Part 5 - Association Rule Learning/Market_Basket_Optimisation.csv', header = None)
transactions = []
for i in range(0, 7501):
    transactions.append([str(dataset.values[i,j]) for j in range(0, 20)])


# Training Apriori on the dataset
from apyori import apriori 
# min_support       = n_prod * 7 (days) / number of transactions
# min_confidence    = confidence too high just gives the obvious
# min_lift          = 
# min_length        = min number of products to check rules
rules = apriori(transactions, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_Length = 2)

# Visualising the results
results = list(rules)
